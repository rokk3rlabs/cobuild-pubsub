const { PubSub } = require('@google-cloud/pubsub');

class PubsubClient {

	constructor(config) {
		if (!config || !config.projectId)
			throw 'No project id provided';

		this.projectId = config.projectId || '';

		const projectId = this.projectId;
		this.client = new PubSub({ projectId });
	}

	/**
	 * Create or get pubsub topic 
	 *
	 * @param {String} name - Topic name
	 * @returnSuccess {Object} Pubsub topic
	 * @returnError {Object} Default pubsub error
	 */
	async createOrGetTopic(name) {
		try {
			await this.client.createTopic(name);
			return await this.client.topic(name);
		}
		catch (error) {
			if (error && error.code === 6)
				return this.client.topic(name);

			return Promise.reject(error);
		}
	}

	/**
	 * Add data to a queue
	 *
	 * @param {Object} topic - Pubsub topic
	 * @param {Object} payload - Data to add 
	 * 
	 * @returnSuccess {Object} Success message 
	 * @returnError {Object} Default pubsub error
	 */
	async publisher(topic, payload) {
		try {
			const messageBuffer = Buffer.from(JSON.stringify(payload));
			const publish = await topic.publish(messageBuffer);
			return {
				message: 'queued'
			};
		}
		catch (error) {
			return Promise.reject(error);
		}
	}

	/**
	 * Create or get a subscription
	 *
	 * @param {Object} topic Pubsub topic
	 * @param {String} name Subscription name
	 * 
	 * @returnSuccess {Object} Pubsub subscription
	 * @returnError {Object} Default pubsub error
	 */
	async createOrGetSubscription(topic, name) {
		try {
			const subscripition = await topic.createSubscription(name);
			return subscripition;
		}
		catch (error) {
			if (error && error.code === 6)
				return await topic.subscription(name);
			return Promise.reject(error);
		}
	}

}

module.exports = PubsubClient;